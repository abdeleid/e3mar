<?php

return [
    'role_structure' => [
        'super_admin' => [
        ],
    ],
    'permission_structure' => [

    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
