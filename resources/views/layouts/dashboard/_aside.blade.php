<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-th"></i><span>@lang('site.main')</span></a></li>

            <li><a href="{{ route('dashboard.developers.index') }}"><i class="fa fa-th"></i><span>@lang('site.developers')</span></a></li>

            {{--project tree--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>@lang('site.projects')</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('dashboard.countries.index') }}"><i class="fa fa-circle-o"></i> @lang('site.countries')</a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.states.index') }}"><i class="fa fa-circle-o"></i> @lang('site.states')</a>
                    </li>
                </ul>
            </li>

            {{--real estate tree--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>@lang('site.real_estate')</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('dashboard.amenities.index') }}"><i class="fa fa-circle-o"></i> @lang('site.amenities')</a>
                        <a href="{{ route('dashboard.districts.index') }}"><i class="fa fa-circle-o"></i> @lang('site.districts')</a>
                    </li>
                </ul>
            </li>

            {{--setting tree--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>@lang('site.settings')</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('dashboard.site_settings.create') }}"><i class="fa fa-circle-o"></i> @lang('site.site_settings')</a>
                    </li>
                </ul>
            </li>

        </ul>

    </section>

</aside>

