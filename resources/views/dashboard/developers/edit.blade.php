@extends('layouts.dashboard.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">

            <h1>@lang('site.developers')</h1>

            <ol class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> @lang('site.main')</a></li>
                <li><a href="{{ route('dashboard.developers.index') }}">@lang('site.developers')</a></li>
                <li class="active">@lang('site.edit')</li>
            </ol>
        </section>

        <section class="content">

            <form action="{{ route('dashboard.developers.update', $developer->id) }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="box box-primary">

                    <div class="box-header with-border">

                        <h3 class="box-title">@lang('site.edit')</h3>

                    </div><!-- end of box header -->

                    <div class="box-body ">

                        @include('dashboard.partials._errors')

                        {{--name--}}
                        <div class="form-group">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{ $developer->name }}">
                        </div>

                        {{--details--}}
                        <div class="form-group">
                            <label>@lang('site.details')</label>
                            <textarea name="details" class="form-control">{{ $developer->details }}</textarea>
                        </div>

                        {{--logo preview--}}
                        <div class="form-group">
                            <img src="{{ $developer->logo_path }}" class="img-thumbnail" style="width: 100px" alt="">
                        </div>

                        {{--logo--}}
                        <div class="form-group">
                            <label>@lang('site.logo')</label>
                            <input type="file" name="logo" class="form-control">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> @lang('site.edit')</button>
                        </div>

                    </div><!-- end of box body -->

                </div><!-- end of box -->

            </form><!-- end of form -->

            <div class="box box-primary">

                <div class="box-header with-border">

                    <h3 class="box-title">@lang('site.gallery_images')</h3>

                </div><!-- end of box header -->


                <div class="box-body">

                    <form action="{{ route('dashboard.developers.upload_gallery_images', $developer->id) }}" id="dz" class="dropzone" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="fallback">
                            <input name="file" type="file" multiple/>
                        </div>
                    </form>

                    <div class="row" id="gallery-images" style="margin-top: 20px">
                        @foreach ($developer->gallery_images as $image)
                            <div class="col-md-3" style="margin-top: 10px">
                                <div class="gallery-image-container">
                                    <img src="{{ asset('uploads/' . $image->name ) }}" class="img-thumbnail" alt=""/>
                                    <div class="overlay">
                                        <form action="{{ route('dashboard.gallery_images.delete', $image->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="btn btn-danger">@lang('site.delete')</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div><!-- end of box body -->

            </div><!-- end of box -->

        </section>

    </div><!-- end of content wrapper -->

@endsection
