@extends('layouts.dashboard.app')

@section('content')
    <div class="content-wrapper">

        <section class="content-header">

            <h1>@lang('site.main')
            </h1>

            <ol class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> @lang('site.main')</a></li>
            </ol>
        </section>

        <section class="content">

            <h1>@lang('site.main')</h1>

        </section><!-- end of content -->
    </div><!-- end of content wrapper -->
@endsection