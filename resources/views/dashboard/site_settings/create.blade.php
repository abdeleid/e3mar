@extends('layouts.dashboard.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">

            <h1>@lang('site.site_settings')</h1>

            <ol class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> @lang('site.main')</a></li>
                <li class="active">@lang('site.site_settings')</li>
            </ol>
        </section>

        <section class="content">

            <form action="{{ route('dashboard.site_settings.store') }}" method="post">

                {{ csrf_field() }}
                {{ method_field('post') }}

                <div class="box box-primary">

                    <div class="box-header with-border">

                        <h3 class="box-title">@lang('site.add')</h3>

                    </div><!-- end of box header -->

                    <div class="box-body ">

                        @include('dashboard.partials._errors')

                        {{--site name--}}
                        <div class="form-group">
                            <label>@lang('site.site_name')</label>
                            <input type="text" name="site_name" class="form-control" value="{{ setting('site_name') }}">
                        </div>

                        {{--site description--}}
                        <div class="form-group">
                            <label>@lang('site.site_description')</label>
                            <textarea name="site_description" class="form-control">{{ setting('site_description') }}</textarea>
                        </div>

                        {{--site link--}}
                        <div class="form-group">
                            <label>@lang('site.site_link')</label>
                            <input type="text" name="site_link" class="form-control" value="{{ setting('site_link') }}">
                        </div>

                        {{--facebook link--}}
                        <div class="form-group">
                            <label>@lang('site.facebook_link')</label>
                            <input type="text" name="facebook_link" class="form-control" value="{{ setting('facebook_link') }}">
                        </div>

                        {{--twitter link--}}
                        <div class="form-group">
                            <label>@lang('site.twitter_link')</label>
                            <input type="text" name="twitter_link" class="form-control" value="{{ setting('twitter_link') }}">
                        </div>

                        {{--linked in link--}}
                        <div class="form-group">
                            <label>@lang('site.linkedin_link')</label>
                            <input type="text" name="linkedin_link" class="form-control" value="{{ setting('linkedin_link') }}">
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="maintenance_mode" {{ setting('maintenance_mode') == 1 ? 'checked' : '' }} value="1"> @lang('site.maintenance_mode')
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>@lang('site.maintenance_text')</label>
                            <textarea name="maintenance_text" class="form-control">{{ setting('maintenance_text') }}</textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                        </div>

                    </div><!-- end of box body -->

                </div><!-- end of box -->

            </form><!-- end of form -->

        </section>

    </div><!-- end of content wrapper -->

@endsection
