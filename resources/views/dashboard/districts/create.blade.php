@extends('layouts.dashboard.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">

            <h1>@lang('site.districts')</h1>

            <ol class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> @lang('site.main')</a></li>
                <li><a href="{{ route('dashboard.districts.index') }}">@lang('site.districts')</a></li>
                <li class="active">@lang('site.add')</li>
            </ol>
        </section>

        <section class="content">

            <form action="{{ route('dashboard.districts.store') }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}
                {{ method_field('post') }}

                <div class="box box-primary">

                    <div class="box-header with-border">

                        <h3 class="box-title">@lang('site.add')</h3>

                    </div><!-- end of box header -->

                    <div class="box-body ">

                        @include('dashboard.partials._errors')

                        {{--name--}}
                        <div class="form-group">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                        </div>

                        {{--details--}}
                        <div class="form-group">
                            <label>@lang('site.details')</label>
                            <textarea name="details" class="form-control ckeditor" id="editor">{{ old('details') }}</textarea>
                        </div>

                        {{--image--}}
                        <div class="form-group">
                            <label>@lang('site.image')</label>
                            <input type="file" name="image" class="form-control">
                        </div>

                        {{--map image--}}
                        <div class="form-group">
                            <label>@lang('site.map_image')</label>
                            <input type="file" name="map_image" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.amenities')</label>
                            <div class="row">
                                @foreach ($amenities as $amenity)
                                  <div class="col-md-2">
                                      <div class="checkbox">
                                          <label>
                                              <input type="checkbox" name="amenities[]" value="{{ $amenity->id }}"> {{ $amenity->name }}
                                          </label>
                                      </div>
                                  </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                        </div>

                    </div><!-- end of box body -->

                </div><!-- end of box -->

            </form><!-- end of form -->

        </section>

    </div><!-- end of content wrapper -->

@endsection
