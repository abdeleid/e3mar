<?php

return [
    'main' => 'الرئسيه',
    'logout' => 'تسجيل الخروج',
    'no_records' => 'للاسف لا يوجد اي سجلات',

    'added_successfully' => 'تم اضافه البيانات بنجاح',
    'updated_successfully' => 'تم اعديل البيانات بنجاح',
    'deleted_successfully' => 'تم حذف البيانات بنجاح',

    'confirm_delete' => 'تاكيد الحذف',
    'yes' => 'نعم',
    'no' => 'لا',

    'search' => 'بحث',
    'add' => 'اضافه',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'action' => 'اكشن',

    'developers' => 'الموطورين',
    'gallery_images' => 'معرض الصور',
    'drop_images' => 'اضعط او قم بسحب الصور هنا',
    'name' => 'الاسم',
    'details' => 'التفاصيل',
    'logo' => 'اللوجو',

    'countries' => 'البلاد',
    'country' => 'البلد',

    'states' => 'المدن',
    'states_count' => 'عدد المدن',

    'projects' => 'المشاريع',

    'real_estate' => 'العقارات',

    'amenities' => 'المميزات',
    'amenities_count' => 'عدد المميزات',

    'medical' => 'طبيه',
    'not_medical' => 'غير طبيه',

    'districts' => 'الاحياء',
    'districts_count' => 'عدد الاحياء',
    'image' => 'صوره',
    'map_image' => 'صوره الخريطه',

    'settings' => 'الاعدادات',
    'site_settings' => 'اعدادات الموقع',
    'site_name' => 'اسم الموقع',
    'site_description' => 'وصف الموقع',
    'site_link' => 'رابط الموقع',
    'facebook_link' => 'رابط facebook',
    'twitter_link' => 'رابط twitter',
    'linkedin_link' => 'رابط linkedin',
    'maintenance_mode' => 'وضع الصيانه',
    'maintenance_text' => 'رساله الصيانه',

];