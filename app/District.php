<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $guarded = [];
    protected $appends = ['image_path', 'map_image_path'];

    public function getImagePathAttribute()
    {
        return asset('uploads/' . $this->image);

    }//end of get image path

    public function getMapImagePathAttribute()
    {
        return asset('uploads/' . $this->map_image);

    }//end of get image path

    public function amenities()
    {
        return $this->belongsToMany(Amenity::class, 'district_amenity');

    }//end of amenities

}//end of model
