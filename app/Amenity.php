<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    protected $guarded = [];

    public function districts()
    {
        return $this->belongsToMany(District::class, 'district_amenity');

    }//end of districts

}//end of model
