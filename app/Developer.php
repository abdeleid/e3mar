<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $guarded = [];

    protected $appends = ['logo_path'];

    public function getLogoPathAttribute()
    {
        return asset('uploads/' . $this->logo);

    }//end of get logo path attribute
    
    public function gallery_images()
    {
        return $this->morphMany(GalleryImage::class, 'galleryable');

    }//end of gallery photos

}//end of model
