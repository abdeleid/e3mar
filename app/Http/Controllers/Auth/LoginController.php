<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();

    }//end of redirect to facebook

    public function handleFacebookCallback()
    {
        try {

            $socialUser = Socialite::driver('facebook')->user();

        } catch (\Exception $e) {

            return redirect()->route('home');

        }//end of catch

        $user = User::where('facebook_id', $socialUser->getId())->first();

        if (!$user) {

            $user = User::create([
                'facebook_id' => $socialUser->getId(),
                'name' => $socialUser->getName(),
                'email' => $socialUser->getEmail(),
                'image' => $socialUser->getAvatar(),
                'verified' => 1,
            ]);

        }//end of if

        Auth::login($user, true);

        return redirect()->intended('/');

    }//end of handle facebook callback

}//end of controller
