<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MaintenanceController extends Controller
{
    public function index()
    {
        if (setting('maintenance_mode') == 1) {

            return view('maintenance.index');

        } else {

            return redirect()->route('welcome');

        }//end of else

    }//end of index

}//end of controller
