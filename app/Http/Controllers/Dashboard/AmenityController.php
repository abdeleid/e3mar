<?php

namespace App\Http\Controllers\Dashboard;

use App\Amenity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AmenityController extends Controller
{
    public function index(Request $request)
    {
        $amenities = Amenity::when($request->search, function($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.amenities.index', compact('amenities'));

    }//end of index

    public function create()
    {
        return view('dashboard.amenities.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        
        $request_data = $request->all();
        $request_data['medical'] = $request->medical ?? 0;
        
        Amenity::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.amenities.index');
        
    }//end of store
    
    public function edit(Amenity $amenity)
    {
        return view('dashboard.amenities.edit', compact('amenity'));
        
    }//end of edit

    public function update(Request $request, Amenity $amenity)
    {
        $request->validate([
            'name' => 'required',
        ]);
        
        $request_data = $request->all();
        $request_data['medical'] = $request->medical ?? 0;
        $amenity->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.amenities.index');
        
    }//end of edit

    public function destroy(Amenity $amenity)
    {
        $amenity->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.amenities.index');

    }//end of delete

}//end of controller
