<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class EditorController extends Controller
{
    public function upload_image(Request $request)
    {
        $CKEditor = Input::get('CKEditor');
        $funcNum = Input::get('CKEditorFuncNum');
        $message = $url = '';

        if (Input::hasFile('upload')) {

            $file = Input::file('upload');

            if ($file->isValid()) {

                $request->file('upload')->store('/editor_images', 'public_uploads');

                $url = asset('/uploads/editor_images/' . $request->file('upload')->hashName());

            } else {

                $message = 'An error occured while uploading the file.';

            }

        } else {
            $message = 'No file uploaded.';
        }

        echo "<script>window.parent.CKEDITOR.tools.callFunction(1,'{$url}','')</script>";

    }//end fo upload images

}//end of controller
