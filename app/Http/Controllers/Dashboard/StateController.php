<?php

namespace App\Http\Controllers\Dashboard;

use App\Country;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPUnit\Framework\Constraint\Count;

class StateController extends Controller
{
    public function index(Request $request)
    {
        $states = State::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.states.index', compact('states'));

    }//end of index

    public function create()
    {
        $countries = Country::all();
        return view('dashboard.states.create', compact('countries'));

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'country_id' => 'required|exists:countries,id',
            'name' => 'required',
        ]);

        State::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.states.index');

    }//end of store

    public function edit(State $state)
    {
        $countries = Country::all();
        return view('dashboard.states.edit', compact('countries', 'state'));

    }//end of edit

    public function update(Request $request, State $state)
    {
        $request->validate([
            'name' => 'required',
            'country_id' => 'required|exists:countries,id',
        ]);

        $state->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.states.index');

    }//end of update

    public function destroy(State $state)
    {
        $state->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.states.index');

    }//end of destroy

}
