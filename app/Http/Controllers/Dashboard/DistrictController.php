<?php

namespace App\Http\Controllers\Dashboard;

use App\Amenity;
use App\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DistrictController extends Controller
{
    public function index()
    {
        $districts = District::paginate(5);
        return view('dashboard.districts.index', compact('districts'));

    }//end of index

    public function create()
    {
        $amenities = Amenity::all();
        return view('dashboard.districts.create', compact('amenities'));

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'details' => 'required',
        ]);

        $request_data = $request->except(['amenities']);

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        if ($request->map_image) {

            $request->file('map_image')->store('/uploads', 'public_uploads');
            $request_data['map_image'] = $request->map_image->hashName();

        }//end of map image

        $district = District::create($request_data);
        $district->amenities()->attach($request->amenities);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.districts.index');

    }//end of store

    public function edit(District $district)
    {
        $amenities = Amenity::all();
        return view('dashboard.districts.edit', compact('amenities', 'district'));

    }//end of edit

    public function update(Request $request, District $district)
    {
        $request->validate([
            'name' => 'required',
            'details' => 'required',
        ]);

        $request_data = $request->except(['amenities']);

        if ($request->image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $district->image);
            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        if ($request->map_image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $district->image);
            $request->file('map_image')->store('/uploads', 'public_uploads');
            $request_data['map_image'] = $request->map_image->hashName();

        }//end of map image

        $district->update($request_data);
        $district->amenities()->sync($request->amenities);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.districts.index');

    }//end of update

    public function destroy(District $district)
    {
        Storage::disk('public_uploads')->delete('/uploads/' . $district->image);
        Storage::disk('public_uploads')->delete('/uploads/' . $district->map_image);
        $district->delete();

        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.districts.index');
        
    }//end of destroy

}//end of controller
