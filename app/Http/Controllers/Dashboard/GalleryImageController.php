<?php

namespace App\Http\Controllers\Dashboard;

use App\GalleryImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class GalleryImageController extends Controller
{
    public function destroy(GalleryImage $gallery_image)
    {
        Storage::disk('public_uploads')->delete('/uploads/' . $gallery_image->name);
        $gallery_image->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->back();
    
    }//end of gallery image

}//end of controller
