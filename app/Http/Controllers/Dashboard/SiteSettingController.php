<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteSettingController extends Controller
{
    public function create()
    {
        return view('dashboard.site_settings.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'maintenance_text' => 'required_with:maintenance_mode',
        ]);

        $request_data = $request->except(['_method', '_token']);
        $request_data['maintenance_mode'] = $request->maintenance_mode ?? '0';
        setting($request_data)->save();

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.site_settings.create');

    }//end of store

}//end of controller
