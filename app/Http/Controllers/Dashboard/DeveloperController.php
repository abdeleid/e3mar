<?php

namespace App\Http\Controllers\Dashboard;

use App\Developer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DeveloperController extends Controller
{
    public function index(Request $request)
    {
        $developers = Developer::when($request->search, function($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('details', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.developers.index', compact('developers'));

    }//end of index

    public function create()
    {
        return view('dashboard.developers.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'details' => 'required',
            'logo' => 'required|image',
        ]);
        
        $request_data = $request->except(['logo']);
        
        if ($request->logo) {

            $request->file('logo')->store('/uploads', 'public_uploads');
            $request_data['logo'] = $request->logo->hashName();
            
        }//end of if 
        
        Developer::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.developers.index');

    }//end of store

    public function show(Developer $developer)
    {
        //
    }

    public function edit(Developer $developer)
    {
        return view('dashboard.developers.edit', compact('developer'));

    }//end of edit

    public function update(Request $request, Developer $developer)
    {
        $request->validate([
            'name' => 'required',
            'details' => 'required',
            'logo' => 'image',
        ]);

        $request_data = $request->except(['logo']);

        if ($request->logo) {

            Storage::disk('public_uploads')->delete('/uploads/' . $developer->logo);
            $request->file('logo')->store('/uploads', 'public_uploads');
            $request_data['logo'] = $request->logo->hashName();

        }//end of if

        $developer->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.developers.index');

    }//end of update

    public function upload_gallery_images(Request $request, Developer $developer)
    {
        $request->file('file')->store('/uploads', 'public_uploads');
        $image = $developer->gallery_images()->create([
            'name' => $request->file->hashName()
        ]);

        return $image;

    }//end fo upload gallery images

    public function destroy(Developer $developer)
    {
        Storage::disk('public_uploads')->delete('/uploads/' . $developer->logo);
        $developer->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.developers.index');
        
    }//end of delete

}//end of controller
