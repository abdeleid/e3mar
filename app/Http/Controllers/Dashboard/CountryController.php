<?php

namespace App\Http\Controllers\Dashboard;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        $countries = Country::when($request->search, function($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.countries.index', compact('countries'));

    }//end of index

    public function create()
    {
        return view('dashboard.countries.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Country::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.countries.index');

    }//end of store

    public function edit(Country $country)
    {
        return view('dashboard.countries.edit', compact('country'));

    }//end of edit

    public function update(Request $request, Country $country)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $country->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.countries.index');

    }//end of update

    public function destroy(Country $country)
    {
        $country->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.countries.index');

    }//end of destroy

}//end of controller
