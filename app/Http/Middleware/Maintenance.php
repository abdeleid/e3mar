<?php

namespace App\Http\Middleware;

use Closure;

class Maintenance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (setting('maintenance_mode') == 1) {

            return redirect()->route('maintenance.index');

        } else {

            return $next($request);

        }//end of else

    }//end of handle

}//end of middlware
