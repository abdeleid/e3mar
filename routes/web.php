<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['maintenance'])->group(function () {

    Route::get('/', function () {
        return view('welcome');
    })->name('welcome');

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    //facebook login routes
    Route::get('login/facebook', 'Auth\LoginController@redirectToFacebook');
    Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');

});

Route::get('/maintenance', 'MaintenanceController@index')->name('maintenance.index');