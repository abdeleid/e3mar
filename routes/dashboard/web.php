<?php

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['auth', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'role:super_admin']
    ], function () {

    Route::prefix('dashboard')->name('dashboard.')->group(function () {

        Route::get('/index', 'DashboardController@index')->name('index');

        //developer routes
        Route::post('/developers/{developer}/upload_gallery_images', 'DeveloperController@upload_gallery_images')->name('developers.upload_gallery_images');
        Route::resource('developers', 'DeveloperController');

        //gallery image routes
        Route::delete('/gallery_images/{gallery_image}/delete', 'GalleryImageController@destroy')->name('gallery_images.delete');

        //country routes
        Route::resource('countries', 'CountryController');

        //state routes
        Route::resource('states', 'StateController');

        //amenity route
        Route::resource('amenities', 'AmenityController');

        //district route
        Route::resource('districts', 'DistrictController');

        //site setting routes
        Route::resource('site_settings', 'SiteSettingController');

//        Route::post('/editor_file_upload', 'EditorController@upload_image')->name('editor.upload_image');

    });

});